<?php
$koneksi = mysqli_connect("localhost", "root", "", "db_evoting");
$vote = mysqli_query($koneksi, "select id_calon from vote order by id");
$daftarCalon = mysqli_query($koneksi, "SELECT nama,foto from calon order by id_calon");
$calon1 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total1 from vote where id_calon=1");
$calon2 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total2 from vote where id_calon=2");
$calon3 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total3 from vote where id_calon=3");
$calon4 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total4 from vote where id_calon=4");
$calon5 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total5 from vote where id_calon=5");
$calon6 = mysqli_query($koneksi, "SELECT COUNT(id_calon) as total6 from vote where id_calon=6");

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-voting</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />

    <script src="js/Chart.js"></script>
    <style type="text/css">
        .bar {
            width: 40%;
            margin-top: 100px;
            margin: auto;
        }

        .flex-container {
            display: flex;
            /* align-items: stretch; */
            background-color: #f1f1f1;
            margin-left: 20px;
        }

        .flex-container>img {
            width: 124px;
            height: 150px;
            margin: 15px;
        }
    </style>
</head>

<body id="page-top">
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary text-uppercase navbar-shrink fixed top" id="mainNav">
        <div class="container">
            <img src="assets/img/logo.png" class="img-fluid img-logo" alt="Responsive image">
            <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Daftar Kandidat</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="index.php">Kembali</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <div class="col-lg">
            <div class="card">
                <div class="card-body">
                    <canvas id="barchart" style="width: 300px !important;" height="150"></canvas>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="col-lg">
                <div class="card">
                    <div class="card-body">
                        <div class="flex-container">
                            <img src="assets/img/Abdil.jpeg" alt="Abdil">
                            <img src="assets/img/feby.jpeg" alt="feby">
                            <img src="assets/img/fotocalon1.jpeg" alt="feby">
                            <img src="assets/img/fotocalon2.jpeg" alt="feby">
                            <img src="assets/img/feri.jpeg" alt="feby">
                            <img src="assets/img/virgi.jpeg" alt="feby">
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>

<script type="text/javascript">
    var ctx = document.getElementById("barchart").getContext("2d");
    var data = {
        labels: [<?php while ($namaCalon = mysqli_fetch_array($daftarCalon)) {
                        echo '"' . $namaCalon['nama'] . '",';
                    } ?>],
        datasets: [{
            label: "Perolehan Suara",
            data: [
                <?php while ($hasilVote1 = mysqli_fetch_assoc($calon1)) :
                    echo '"' . $hasilVote1['total1'] . '",';
                ?>
                <?php endwhile; ?>
                <?php while ($hasilVote2 = mysqli_fetch_assoc($calon2)) :
                    echo '"' . $hasilVote2['total2'] . '",';
                ?>
                <?php endwhile; ?>
                <?php while ($hasilVote3 = mysqli_fetch_assoc($calon3)) :
                    echo '"' . $hasilVote3['total3'] . '",';
                ?>
                <?php endwhile; ?>
                <?php while ($hasilVote4 = mysqli_fetch_assoc($calon4)) :
                    echo '"' . $hasilVote4['total4'] . '",';
                ?>
                <?php endwhile; ?>
                <?php while ($hasilVote5 = mysqli_fetch_assoc($calon5)) :
                    echo '"' . $hasilVote5['total5'] . '",';
                ?>
                <?php endwhile; ?>
                <?php while ($hasilVote6 = mysqli_fetch_assoc($calon6)) :
                    echo '"' . $hasilVote6['total6'] . '",';
                ?>
                <?php endwhile; ?>
            ],
            backgroundColor: [
                "rgba(205, 45, 24,0.75)",
                "rgba(248, 194, 77,0.75)",
                "rgba(248, 245, 77,0.75)",
                "rgba(80, 248, 77,0.75)",
                "rgba(77, 80, 248,0.75)",
                "rgba(220, 77, 248,0.75)",
            ],
            borderColor: [
                "rgba(38, 185, 154, 0.7)",
            ],
            pointBorderColor: [
                "rgba(38, 185, 154, 0.7)"
            ],
            pointBackgroundColor: [
                "rgba(38, 185, 154, 0.7)"
            ],
            pointHoverBackgroundColir: [
                "#fff"
            ],
            pointHoverBorderColor: [
                "rgba(220,220,220,1)"
            ],
        }]
    };
    var myBarchart = new Chart(ctx, {
        type: 'bar',
        data: data,
        options: {
            legend: {
                display: false
            },
            barValueSpacing: 20,
            scales: {
                yAxes: [{
                    ticks: {
                        min: 0,
                    }
                }],
                xAxes: [{
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            }
        }
    })
</script>