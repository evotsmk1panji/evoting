<?php
session_start();
require 'functions.php';
hasLogin();

if(isset($_SESSION["login"]) ){
    header("Location: index.php");
    exit;
}

if(isset($_POST["login"]) ) {

    $id_user = $_POST["id_user"];
    $password = $_POST["password"];

    $result = mysqli_query($conn, "SELECT * FROM user WHERE id_user = '$id_user'");

    // cek username 
    if( $result->num_rows > 0 ) {

        //cek password
        $row = mysqli_fetch_assoc($result);
        if($password == $row["password"]) {
            // set session
            $_SESSION["login"] = 'true';
            header("Location: pilih.php");
            exit;
        }
    }
    $error = true;
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="Rafle">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>

    <!-- Bootstrap CSS v4.1.x -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Fontsawesome CDN -->
    <script src="https://use.fontawesome.com/e69b0efea7.js"></script>
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;900&display=swap" rel="stylesheet">

    <!-- Refle CSS -->
    <link rel="stylesheet" href="css/refle.css">
</head>

<body class="refle-login">
    <section class="h-100">
        <div class="container h-100">
            <div class="row justify-content-md-center h-100">
                <div class="card-wrapper">
                    <h2 class="title text-center">
                        E-voting <br> SMKN 1 Panji
                    </h2>
                <?php if(isset($error) ) : ?>
                    <p class="text-center" style="color: red; font-style: italic; font-size: 18px;">Username/Password salah!</p>
                <?php endif; ?>
                    <div class="card fat">
                        <div class="card-body">
                            <form method="post" class="needs-validation" novalidate>
                                <div class="form-group">
                                    <label for="id_user">ID</label>
                                    <input id="id_user" type="text" class="form-control" name="id_user" 
                                           placeholder="Masukkan ID" value="" required autofocus>
                                    <div class="invalid-feedback">
                                        ID is invalid
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="password">Password</label>
                                    <div class="input-group mb-8">
                                        <input id="password" type="password" class="form-control" name="password"
                                            placeholder="Masukkan Password" required>
                                        <div class="input-group-append">
                                            <span class="btn btn-primary text-white input-group-text">
                                                <i id="password" class="toggle-password fas fa-eye"></i>
                                            </span>
                                        </div>
                                        <div class="invalid-feedback">
                                            Password is required
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <button name="login" type="submit" class="btn btn-sm btn-primary btn-block">
                                        Login
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="footer">
                        Copyright &copy; 2020 &mdash; Tim RPL SMKN 1 Panji
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/jquery-3.5.0.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/refle.js"></script>
</body>

</html>