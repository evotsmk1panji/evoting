<?php
require 'functions.php';
$query = "SELECT * FROM calon";
$rows = query($query);


?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-voting</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/img/osis.png" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.13.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link href="css/custom.css" rel="stylesheet" />
</head>

<body id="page-top">
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg bg-secondary text-uppercase navbar-shrink fixed-top" id="mainNav">
        <div class="container">
            <img src="assets/img/logo.png" class="img-fluid img-logo" alt="Responsive image">
            <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#portfolio">Kandidat</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="pilih.php">Vote</a></li>
                    <li class="nav-item mx-0 mx-lg-1"><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="#hasil">Hasil</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead bg-primary text-white text-center">
        <div class="container d-flex align-items-center flex-column">
            <!-- Masthead Avatar Image-->
            <img class="masthead-avatar mb-5 rounded-circle" src="assets/img/osis.png" alt="" />
            <!-- Masthead Heading-->
            <h1 class="masthead-heading text-uppercase mb-0">E-Voting</h1>
            <!-- Icon Divider-->
            <div class="divider-custom divider-light">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <!-- Masthead Subheading-->
            <p class="masthead-subheading font-weight-light mb-0">Pemilihan Ketua OSIS SMKN 1 Panji</p>
            <p class="masthead-subheading font-weight-light mb-0">Masa Bakti 2020-2021</p>
        </div>
    </header>
    <!-- Portfolio Section-->
    <section class="page-section portfolio" id="portfolio">
        <div class="container">
            <!-- Portfolio Section Heading-->
            <p class="page-section-heading text-center text-uppercase text-secondary mb-0">Kandidat Ketua OSIS Masa Bakti 2020-2021</p>
            <!-- Icon Divider-->
            <div class="divider-custom">
                <div class="divider-custom-line"></div>
                <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                <div class="divider-custom-line"></div>
            </div>
            <p class="text-center text-uppercase text-secondary mb-0">Pilih gambar kandidat untuk melihat detil</p><br>
            <!-- Portfolio Grid Items-->
            <div class="row">
                <!-- Portfolio Item 1-->
                <?php $i = 1; ?>
                <?php foreach ($rows as $calon) : ?>
                    <div class="col-md-6 col-lg-4 mb-5">
                        <div class="portfolio-item mx-auto" data-kandidat="<?= $i; ?>" data-toggle="modal" data-target="#votingModal<?= $i; ?>">
                            <div class="portfolio-item-caption d-flex align-items-center justify-content-center h-100 w-100">
                                <div class="portfolio-item-caption-content text-center text-white">Kandidat <?= $i ?></i></div>
                            </div>
                            <img class="img-fluid" src="assets/img/portfolio/<?= $calon['foto'] ?>" alt="" />
                        </div>
                    </div>

                    <div class="portfolio-modal modal fade" id="votingModal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="votingModal" aria-hidden="true">
                        <div class="modal-dialog modal-xl" role="document">
                            <div class="modal-content">
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true"><i class="fas fa-times"></i></span>
                                </button>
                                <div class="modal-body text-center">
                                    <div class="container">
                                        <div class="row justify-content-center">
                                            <div class="col-lg-8">
                                                <!-- Portfolio Modal - Title-->
                                                <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal1Label"><?= $calon['nama'] ?></h2>
                                                <!-- Icon Divider-->
                                                <div class="divider-custom">
                                                    <div class="divider-custom-line"></div>
                                                    <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                                    <div class="divider-custom-line"></div>
                                                </div>
                                                <!-- Portfolio Modal - Image-->
                                                <img class="img-fluid rounded mb-5" src="assets/img/portfolio/<?= $calon['foto'] ?>" alt="" />
                                                <!-- Portfolio Modal - Text-->
                                                <h2>Visi</h2>
                                                <p><?= $calon['visi'] ?></p>
                                                <h2>Misi</h2>
                                                <p><?= $calon['misi'] ?></p>
                                                <video id="vid1" heigth="480" controls>
                                                    <source src="assets/vid/kandidat1s.mp4" type="video/mp4">
                                                    Maaf, browser Anda tidak mendukung video.
                                                </video>
                                                <br>
                                                <br>
                                                <button class="btn btn-primary" data-dismiss="modal">
                                                    <i class="fas fa-times fa-fw"></i>
                                                    Close Window
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php $i++; ?>
                <?php endforeach ?>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="row">
            <div class="col">
                <a href="login.php" class="btn btn-primary btn-lg btn-block">Vote</a>
            </div>
            <div class="col">
                <a href="hasil.php" class="btn btn-primary btn-lg btn-block">Hasil</a>
            </div>
        </div>
        <br>
        <!-- Copyright Section-->
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright © 2020 Tim RPL SMKN 1 Panji</small></div>
        </div>
        <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes)-->
        <div class="scroll-to-top d-lg-none position-fixed">
            <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top"><i class="fa fa-chevron-up"></i></a>
        </div>
        <!-- Portfolio Modals-->
        <!-- Bootstrap core JS-->
        <script src="./js/jquery.min.js"></script>
        <script src="./js/bootstrap.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- <script type="text/javascript">
            $('.portfolio-item').on('click', function(e) {
                e.preventDefault
                let kandidatId = $(this).data('kandidat')
                let kandidatModal = $(this).data('target')

                $.ajax({
                    url: './utils/modals.php',
                    type: 'POST',
                    data: {
                        id: kandidatId
                    },
                    success: function(res) {
                        console.log(res);
                    }
                });
            });
        </script> -->
</body>

</html>