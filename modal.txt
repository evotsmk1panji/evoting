
        <!-- Portfolio Modal 1-->
        <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-labelledby="portfolioModal1Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal1Label">Log Cabin</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/profile1.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                     <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td><b>Nama</b></td>
                                              <td><?php print_r($rows); ?></td>
                                            </tr>
                                            <tr>
                                              <td><b>Visi</b></td>
                                              <td>Menjadikan siswa siswi SMKN 1 PANJI yg UPDATE (Unggul, Peduli, Disiplin, Aktif, Tegas, dan Edukatif), serta menjadikan osis sebagai sarana untuk menampung inspirasi dan aspirasi siswa dalam berbagai kegiatan.</td>
                                            </tr>
                                            <tr>
                                              <td><b>Misi</b></td>
                                              <td> 
                                                <li>Meningkatkan keimanan dan ketakwaan kepada tuhan yg maha esa.</li> 
                                                <li>Mengembangkan Kreatifitas, Bakat, Minat dan potensi siswa melalui kegiatan extrakulikuler.</li>
                                                <li>Merencanakan dan melaksanakan segala program osis dengan prinsip kerjasama, kerjacerdas, kerja ikhlas dan kerja tuntas.</li>
                                                <li>Meningkatkan kedisiplinan siswa dalam berbagai kegiatan.</li>
                                                <li>Meningkatkan kembali kesadaran siswa mengenai pentingnya menjaga lingkungan sekolah.</li>
                                                <li>Meningkatkan jiwa nasionalisme kepada siswa siswi SMKN 1 PANJI.</li>
                                               </td>
                                            </tr>
                                            <tr>
                                            </tr>
                                          </tbody>
                                        </table>
                                        <video id="vid1" heigth="480" controls>
                                                    <source src="assets/vid/portfolio/kandidat1s.mp4" type="video/mp4">
                                                    Maaf, browser Anda tidak mendukung video.
                                                </video>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 2-->
        <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-labelledby="portfolioModal2Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal2Label">Tasty Cake</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/cake.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td>Nama</td>
                                              <td>Lorem ipsum dolor sit amet</td>
                                            </tr>
                                            <tr>
                                              <td>Visi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                            <tr>
                                              <td>Misi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 3-->
        <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-labelledby="portfolioModal3Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal3Label">Circus Tent</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/circus.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td>Nama</td>
                                              <td>Lorem ipsum dolor sit amet</td>
                                            </tr>
                                            <tr>
                                              <td>Visi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                            <tr>
                                              <td>Misi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 eeqq347777777777777       </div>
        <!-- Portfolio Modal 4-->
        <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-labelledby="portfolioModal4Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal4Label">Controller</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/game.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                     <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td>Nama</td>
                                              <td>Lorem ipsum dolor sit amet</td>
                                            </tr>
                                            <tr>
                                              <td>Visi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                            <tr>
                                              <td>Misi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 5-->
        <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-labelledby="portfolioModal5Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal5Label">Locked Safe</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/safe.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                     <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td>Nama</td>
                                              <td>Lorem ipsum dolor sit amet</td>
                                            </tr>
                                            <tr>
                                              <td>Visi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                            <tr>
                                              <td>Misi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Portfolio Modal 6-->
        <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-labelledby="portfolioModal6Label" aria-hidden="true">
            <div class="modal-dialog modal-xl" role="document">
                <div class="modal-content">
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><i class="fas fa-times"></i></span>
                    </button>
                    <div class="modal-body text-center">
                        <div class="container">
                            <div class="row justify-content-center">
                                <div class="col-lg-8">
                                    <!-- Portfolio Modal - Title-->
                                    <h2 class="portfolio-modal-title text-secondary text-uppercase mb-0" id="portfolioModal6Label">Submarine</h2>
                                    <!-- Icon Divider-->
                                    <div class="divider-custom">
                                        <div class="divider-custom-line"></div>
                                        <div class="divider-custom-icon"><i class="fas fa-star"></i></div>
                                        <div class="divider-custom-line"></div>
                                    </div>
                                    <!-- Portfolio Modal - Image-->
                                    <img class="img-fluid rounded mb-5" src="assets/img/portfolio/submarine.png" alt="" />
                                    <!-- Portfolio Modal - Text-->
                                     <div class="table-responsive">
                                        <table class="table table-striped">
                                          <tbody>
                                            <tr>
                                              <td>Nama</td>
                                            <td>Lorem ipsum dolor sit amet</td>
                                            </tr>
                                            <tr>
                                              <td>Visi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                            <tr>
                                              <td>Misi</td>
                                              <td>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                              tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                              quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                              consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</td>
                                            </tr>
                                          </tbody>
                                        </table>
                                    </div>
                                    <button class="btn btn-primary" data-dismiss="modal">
                                        <i class="fas fa-times fa-fw"></i>
                                        Close Window
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
