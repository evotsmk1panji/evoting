<?php 
// Conect to database
$conn = mysqli_connect("localhost","root","","db_evoting"); 


function query($query) {
	global $conn;
	$result = mysqli_query($conn, $query);
	$rows = [];
	while ( $row = mysqli_fetch_assoc($result) ) {
		$rows[] = $row;
	}
	return $rows;
}

function hasLogin() {
	if(isset($_SESSION["login"])) {
		header("Location: index.php");
	    exit;
	}
}